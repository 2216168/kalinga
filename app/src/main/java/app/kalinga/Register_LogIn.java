package app.kalinga;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class Register_LogIn extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_login);


        Button signUp = findViewById(R.id.sign_up);
        signUp.setOnClickListener(v -> openSignUpView());

        Button logIn = findViewById(R.id.log_in_button);
        logIn.setOnClickListener(x -> openWelcomeView());

    }

    public void openSignUpView() {
        Intent intent = new Intent(this, CreateAccount.class);
        startActivity(intent);
    }

    public void openWelcomeView() {
        Intent intent = new Intent(this, Welcome.class);
        startActivity(intent);
    }
}