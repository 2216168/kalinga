package app.kalinga;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import androidx.appcompat.app.AppCompatActivity;

public class PatientVerification extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_verification);

        View proceedButton = findViewById(R.id.proceedButton);
        proceedButton.setOnClickListener(v -> proceedToNext());

        CheckBox checkBox = findViewById(R.id.checkBox);
        checkBox.setOnClickListener(v -> chekIfChecked(checkBox,proceedButton));

    }

    private void proceedToNext() {
        Intent intent = new Intent(this, PatientHome.class);
        startActivity(intent);
    }

    private void chekIfChecked(CheckBox checkBox,View p) {
        if (checkBox.isChecked()){
            p.setClickable(true);
        }else{
            p.setClickable(false);
        }
    }
}
