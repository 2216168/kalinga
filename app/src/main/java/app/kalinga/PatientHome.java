package app.kalinga;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;

public class PatientHome extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_home);

        View patient_view = findViewById(R.id.imageView19);
        patient_view.setOnClickListener(v -> caregiverInfo());

        View patient1_view = findViewById(R.id.imageView20);
        patient1_view.setOnClickListener(v -> caregiverInfo());

        View patient2_view = findViewById(R.id.imageView21);
        patient2_view.setOnClickListener(v -> caregiverInfo());

        View patient3_view = findViewById(R.id.imageView22);
        patient3_view.setOnClickListener(v -> caregiverInfo());

        View patient4_view = findViewById(R.id.imageView23);
        patient4_view.setOnClickListener(v -> caregiverInfo());

        View patient5_view = findViewById(R.id.imageView24);
        patient5_view.setOnClickListener(v -> caregiverInfo());
    }

    private void caregiverInfo() {
        Intent intent = new Intent(this, CaregiverInfo.class);
        startActivity(intent);
    }
}
