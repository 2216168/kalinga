package app.kalinga;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CreateAccount extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_account);

        TextView t2 = (TextView) findViewById(R.id.log_in);
        t2.setOnClickListener(v -> openLogInView());

    }

    public void openLogInView() {
        Intent intent = new Intent(this, Register_LogIn.class);
        startActivity(intent);
    }



}
