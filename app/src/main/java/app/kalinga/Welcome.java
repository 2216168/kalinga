package app.kalinga;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class Welcome  extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

        LinearLayout patientButton = findViewById(R.id.patientButton);
        patientButton.setOnClickListener(v -> patientSignUpView());

        LinearLayout caregiverButton = findViewById(R.id.caregiverButton);
        caregiverButton.setOnClickListener(v -> caregiverSignUpView());




    }

    private void caregiverSignUpView() {
        Button sign_up = findViewById(R.id.sign_up);
        sign_up.setOnClickListener(v -> openSignUpViewC());
    }

    private void patientSignUpView() {
        Button sign_up = findViewById(R.id.sign_up);
        sign_up.setOnClickListener(v -> openSignUpViewP());
    }

    private void openSignUpViewP() {
        Intent intent = new Intent(this, PatientVerification.class);
        startActivity(intent);
    }

    private void openSignUpViewC() {
        Intent intent = new Intent(this, PatientVerification.class);//change to the verification for caregiver
        startActivity(intent);
    }

}
