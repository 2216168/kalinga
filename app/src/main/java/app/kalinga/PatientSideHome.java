package app.kalinga;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class PatientSideHome extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_side_home);

        View profileButton = findViewById(R.id.profileButton);
        profileButton.setOnClickListener(v -> profileButton());


    }

    private void profileButton() {
        Intent intent = new Intent(this, PatientProfile.class);
        startActivity(intent);
    }
}
